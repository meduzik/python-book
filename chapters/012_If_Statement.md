# Инструкция выбора

Сначала дадим определение блоку инструкций в языке Python. **Блоком инструкций** будем называть последовательность инструкций, которые находятся на одном уровне **отступа** (имеют одинаковое число пробелов или символов табуляции слева).

Обычно мы фиксируем “размер отступа”, например, принимая его равным 4 пробелам или 1 табу. Это значит, что когда мы создаем новый блок, мы добавляем к нему еще один отступ, равный этому размеру.

**Инструкция выбора** (которую еще иногда называют **условным оператором**) - наш первый способ создания нелинейного потока управления. В простейшем виде она записывается так:

```python
if условие:
  блок_действий
```

Условный оператор работает следующим образом:

- Вычисляется значение выражения на месте условия.
  - Если это значение является `None`, `False`, числовым нулем, пустой строкой (или еще некоторыми значениями), мы говорим, что **условие не выполнено**.
  - В противном случае (например, `True`, непустая строка, не нулевое число) - что **условие выполнено**.
- Если условие выполнено, то поток управления переходит к первой инструкции в блоке действий.
  - Когда будет выполнена последняя инструкция из блока действий, поток управления перейдет к следующей за условным оператором инструкции.
- Если условие не выполнено, то поток управления переходит к следующей за условным оператором инструкции (минуя блок действий)

Рассмотрим на примере:

```python
name = input("Введите имя: ")

if name == "Вася":
  print("У вас новое письмо")
  
print("Здравствуйте, " + name)
```

- Первая инструкция, как мы знаем, помещает введенную пользователем строку в переменную `name`.
- Затем наступает очередь инструкции выбора.
  - Сначала вычисляется значение выражения в условии (`name == "Вася"`). 
  - Допустим, пользователь ввел строку `Вася`. Тогда значением выражения `name == "Вася"` будет `True`, и условие будет выполнено.
    - Поскольку условие выполнено, поток управления перейдет в блок действий, к инструкции `print("У вас новое письмо")`.
    - Эта инструкция выведет в консоль `"У вас новое письмо"`
    - Больше инструкций в блоке действий нет, так что поток управления перейдет к следующей инструкции после условного оператора, а именно `print("Здравствуйте, " + name)`
    - Таким образом, в консоль будет выведено:
      > У вас новое письмо
      >
      > Здравствуйте, Вася
  - Предположим теперь, что пользователь ввел другую строку (`Ваня`). Тогда значение выражения `name == "Вася"` будет `False`, и условие не будет выполнено.
    - Поскольку условие не выполнено, блок действий будет пропущен и управление сразу перейдет к следующей инструкции `print("Здравствуйте, " + name)`
    - В этом случае в консоли будет только одна строка:
      > Здравствуйте, Ваня

Таким образом, условный оператор позволяет нам выполнять разные последовательности инструкций в зависимости от некоторого состояния программы.