import chapters
import os
import sys
import shutil
import markdown
import transliterate
import re
from markdown.extensions.toc import TocExtension
from weasyprint import HTML, CSS

build_dirname = ".build"


filter_re = re.compile("[^a-zA-Z0-9_\\-]")
sep_re = re.compile("\\s+")


def ensure_dirs(path):
	os.makedirs(os.path.dirname(path), exist_ok=True)


def reset_build_directory():
	if os.path.isdir(build_dirname):
		for root, dirs, files in os.walk(build_dirname):
			for name in files:
				os.remove(os.path.join(root, name))
			for name in dirs:
				shutil.rmtree(os.path.join(root, name))
	else:
		os.makedirs(build_dirname)


def generate_unified_markdown(chapters_list, filename):
	ensure_dirs(filename)
	with open(filename, "w", encoding="UTF-8") as fp:
		for chapter in chapters_list.get_list():
			with open(chapter.path, "r", encoding="UTF-8") as input_fp:
				fp.write(input_fp.read())
				fp.write("\n\n")
		fp.write("\n")


def compile_html(input_filename, output_filename):
	with open(input_filename, "r", encoding="UTF-8") as fp:
		contents = fp.read()

	def slugify(value, separator):
		translit = transliterate.translit(value, "ru", reversed=True)
		filtered = filter_re.sub("", sep_re.sub(separator, translit))
		result = filtered.lower()
		return result

	html = markdown.markdown(contents, extensions=[
		"markdown.extensions.fenced_code",
		"markdown.extensions.tables",
		"markdown.extensions.codehilite",
		"markdown.extensions.nl2br",
		TocExtension(anchorlink=True, slugify=slugify)
	])

	ensure_dirs(output_filename)
	with open(output_filename, "w", encoding="UTF-8") as fp:
		fp.write('<!DOCTYPE html>\n')
		fp.write('<html lang="ru">\n')
		fp.write('<head>\n')
		fp.write('<meta charset="utf-8">\n')
		fp.write('<title>Python Book</title>\n')
		fp.write('<link rel="stylesheet" href="css/base.css">\n')
		fp.write('<link rel="stylesheet" href="css/markdown.css">\n')
		fp.write('<link rel="stylesheet" href="css/codehilite.css">\n')
		fp.write('</head>\n')
		fp.write('<body>\n')
		fp.write('<div class="markdown-container markdown-body">\n')
		fp.write(html)
		fp.write('</div>\n')
		fp.write('</body>\n')
		fp.write('</html>\n')

	output_dir = os.path.dirname(output_filename)

	os.makedirs(os.path.join(output_dir, "css"), exist_ok=True)
	shutil.copyfile("css/github-markdown.css", os.path.join(output_dir, "css", "markdown.css"))
	shutil.copyfile("css/codehilite.css", os.path.join(output_dir, "css", "codehilite.css"))
	shutil.copyfile("css/base.css", os.path.join(output_dir, "css", "base.css"))
	shutil.copytree("chapters/img", os.path.join(output_dir, "img"))


def compile_pdf(html_file, pdf_file):
	HTML(filename=html_file).write_pdf(pdf_file, stylesheets=[CSS(filename="css/weasyprint.css")])


def compile():
	print("Preparing build directory")
	reset_build_directory()

	print("Order chapters")
	chapters_list = chapters.get_all_chapters("chapters")
	chapters_list.reorder()

	print("Generating unified markdown file")
	generate_unified_markdown(chapters_list, build_dirname + "/PythonBook.md")

	print("Generating HTML")
	compile_html(build_dirname + "/PythonBook.md", build_dirname + "/html/index.html")

	print("Generating PDF")
	compile_pdf(build_dirname + "/html/index.html", build_dirname + "/PythonBook.pdf")


compile()