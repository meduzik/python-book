# Значения

**Значениями** будем называть некоторые “объекты”, существующие во время работы программы. Например, числа, строки, массивы, файлы, изображения и кнопки - все это значения. Четкое определение дать сложно, вместо этого ограничимся примерами и классификацией.

У каждого **значения** есть [**тип данных**](https://ru.wikipedia.org/wiki/%D0%A2%D0%B8%D0%BF_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85). Это “класс”, “категория” или “множество”, к которому принадлежит значение. 

Приведем примеры некоторых значений и их типы:

```python
# числа
1    # целое
4.5  # дробное
-6   # целое
-7.0 # дробное

# строки (последовательность символов)
"Hello, world"
"Привет, мир"  

# логические значения
True   # логическая истина, тип логическое (или "булево") значение
False  # логическа ложь

# специальное значение, обозначающее "ничего"
None   # имеет собственный уникальный тип
```

Все, что начинается со знака `#` и до конца строки является комментарием и не оказывает влияния на программу.
  
Проверить тип значения можно командой `type(значение)`.
 Ниже приведена интерактивная сессия с интерпретатором Python. Ввод пользователя начинается с `>>>`, ответы интерпретатора на следующей строке. Чтобы запустить интерактивный режим, нужно ввести в командной строке `python` без параметров (или воспользоваться интерактивным режимом в IDE).  

![Интерактивный режим в командной строке Windows](img/001_001.png)
![Интерактивный режим в PyCharm](img/001_002.png)

```
>>> type(1)
<class 'int'>
>>> type(4.5)
<class 'float'>
>>> type(True)
<class 'bool'>
>>> type("Hello")
<class 'str'>
>>> type(None)
<class 'NoneType'>
```

Мы видим, что каждому значению Python ставит в соответствие какой-то класс (например `float` для дробного числа).

**Вопросы и задания**

 1. Что такое интерактивный режим Python? Для чего его можно использовать?
 2. Какие типы будут у значений `False`, `"Python"`, `-100`, `-100.0`? Проверьте с помощью интерактивного режима.
 3. Логический тип имеет ровно два значения - истина (`True`) и ложь (`False`). Сколько вообще различных значений может быть у типа? Приведите примеры.

