import chapters
import re
import os
import sys
import shutil
import urllib
import urllib.request


image_re = re.compile("!\\[([^\\]]*)\\]\\(([^)]+)\\)")


def main():
	chapters_list = chapters.get_all_chapters("chapters")
	chapters_list.reorder()

	used_images = set()

	os.makedirs("chapters/img", exist_ok=True)

	for idx, chapter in enumerate(chapters_list.get_list()):
		prefix = str("%03d" % (idx + 1))
		lines = []
		with open(chapter.path, "r", encoding="UTF-8") as fp:
			image_idx = 1

			def process_image(match):
				nonlocal image_idx

				target_name = "img/" + prefix + "_" + str("%03d" % image_idx) + ".png"

				uri = match.group(2)
				if uri.startswith("http"):
					# download
					print("Downloading {url}".format(url=uri))
					urllib.request.urlretrieve(uri, os.path.join("chapters", target_name))
				elif uri != target_name:
					# copy
					shutil.copyfile(os.path.join("chapters", uri), os.path.join("chapters", target_name))

				used_images.add(os.path.normpath(os.path.join("chapters", target_name)))

				image_idx += 1
				return "![" + match.group(1) + "](" + target_name + ")"

			for line in fp.readlines():
				lines.append(image_re.sub(process_image, line))

		with open(chapter.path, "w", encoding="UTF-8") as fp:
			fp.writelines(lines)

	for root, dirs, files in os.walk("chapters/img"):
		for name in files:
			if os.path.normpath(os.path.join(root, name)) not in used_images:
				os.remove(os.path.join(root, name))

main()