import sys
import os
import re


md_re = re.compile(".+[.]md")
num_chapter_re = re.compile("[0-9]+_(.+)")
title_re = re.compile("#\s*(.+)")


class Chapters:
	def __init__(self):
		self.list = []

	def add(self, chapter):
		self.list.append(chapter)

	def reorder(self):
		for idx, chapter in enumerate(self.list):
			chapter.assign_number(idx + 1)

	def get_list(self):
		return self.list


class ChapterFile:
	def __init__(self, path):
		self.path = path
		self.base_path = os.path.dirname(path)
		self.name, _ = os.path.splitext(os.path.basename(path))
		num_groups = num_chapter_re.match(self.name)
		if num_groups:
			self.name = num_groups.group(1)

	def assign_number(self, num):
		new_path = os.path.join(self.base_path, ("%03d" % num) + "_" + self.name + ".md")
		if new_path != self.path:
			os.rename(self.path, new_path)
			self.path = new_path

	def extract_title(self):
		with open(self.path, "r", encoding="UTF-8") as fp:
			for line in fp.readlines():
				match = title_re.match(line)
				if match:
					return match.group(1)
		raise RuntimeError("Cannot find title in chapter {name}".format(name=self.name))



def get_all_chapters(base_path):
	md_paths = []

	for root, dirs, files in os.walk(base_path):
		for name in files:
			file_path = os.path.join(root, name)
			if md_re.match(name):
				md_paths.append(file_path)

	md_paths.sort()

	chapters = Chapters()

	for path in md_paths:
		chapters.add(ChapterFile(path))

	return chapters
