# Инструкция raise (базовое использование)

Вы уже скорее всего сталкивались с ошибками в Python. Вы можете **бросать** свои собственные ошибки - если что-то пошло не так.  На самом деле это очень мощный механизм, позволяющий одним частям программы исправлять неожиданные происшествия в других, но мы будем использовать его только в самом базовом варианте - чтобы прервать выполнение программы и сообщить пользователю об ошибке.

Делать мы это будем так:

```python
raise RuntimeError("текст ошибки")
```

Например:

```python
n = int(input("Введите сторону треугольника: "))

if n <= 0:
  raise RuntimeError("Длина стороны должна быть положительной")
```

Такая инструкция завершит выполнение программы и выведет в консоль текст ошибки, а также подробную информацию о том, где произошла ошибка. А в случае, если программа запущена в режиме отладки, она будет остановлена сразу после возникновения ошибки и у вас появится возможность изучить состояние - значения переменных, которое привело к ошибке.

Пользуйтесь инструкцией `raise` там, где не ожидаете, что программа может продолжить корректно работать, вместо, например, вывода текста ошибки с помощью `print`. Это позволит быстрее найти источник ошибки в случае возникновения проблем.